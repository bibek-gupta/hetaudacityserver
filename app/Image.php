<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Image extends Model
{
    use HasApiTokens;
    protected $fillable=[
        'categoryId','profileId','image','mainImage'
    ];

}
