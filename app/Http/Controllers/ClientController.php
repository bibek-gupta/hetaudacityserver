<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

Use App\Category;
Use App\Information;
Use App\FeedBack;
use App\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function getCategory(){
        $category= Category::get();
        return response()->json(['category'=>$category]);
    }



    public function getProfileInformation(Request $request){
        $profileImages = Image::where('mainImage',1)->where('categoryId',$request->id)->groupBy('profileId')->get();
        $profileInformation= Information::where('categoryId',$request->id)->get();
        return response()->json(['profileInformation'=>$profileInformation,'profileImages'=> $profileImages]);
    }



    
    public function getProfileDetails(Request $request){
        $userDetails=Auth::user();
        $userId= Auth::id();
        $details= Information::where('informations.id',$request->id)->get();

        $mainImage = Image ::select('image')->where('profileId',$request->id)
                            ->where('mainImage',1)
                            ->groupBy('profileId')
                            ->get();

        $images  = Image::select('image')
                            ->where('images.profileId',$request->id)
                            ->get();

        $feedback= DB::table('users')
                       -> join('feedbacks','users.id','=','feedbacks.userId')
                       ->where('feedbacks.profileId',$request->id)
                       ->get();

        $userfeedback= FeedBack::where('userId',$userId)
                                ->where('profileId',$request->id)
                                ->get();
        return response()
                        ->json(['mainImage'=>$mainImage,'images'=>$images,'details'=>$details,'feedback'=>$feedback,'userDetails'=>$userDetails,'userfeedback'=>$userfeedback]);
    }



    public function postFeedBack(Request $request){

        $userIdRules= array(
            'userId'=>'unique:FeedBacks',
            
        );
        $profileIdRules= array(
            'profileId'=>'unique:FeedBacks'
        );
        $feedback= new FeedBack;
        $feedback->userId = Auth::id();
        $feedback->ratedStar = $request->star;
        $feedback->profileId = $request->profileId;
        $feedback->feedBack_title = $request->feedback_title;
        $feedback->feedBack = $request->feedback;
        $array=$feedback->toArray();
        $userValidator= Validator::make($array,$userIdRules);
        $profileIdValidator= Validator::make($array,$profileIdRules);
        if($userValidator->fails() ){
            if($profileIdValidator->fails()){
                return response()->json(['feedback'=>'Not allowed']);
            }else{
                $feedback->save();
                return response()->json(['feedback'=>$feedback,'status'=>'true']);
            }
        }else{
            $feedback->save();
             return response()->json(['feedback'=>$feedback,'status'=>'true']);
        }
       

    }
    
    public function getListOfBuildings(){
        $listOfBuildings= Information::get();
        $categoriesLists= DB::table('categories')
                              ->get();
        $images = Image::where('mainImage',1)->groupBy('images.categoryId')->get();
        return response()->json(['listOfBuildings'=>$listOfBuildings,'categoriesList'=> $categoriesLists,'images'=>$images]);
    }
}
