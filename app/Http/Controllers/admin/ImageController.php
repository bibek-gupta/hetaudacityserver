<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;
use Validator;
class ImageController extends Controller
{
   public function uploadImage(Request $request){
           $image= new Image;
           $image->profileId    =       $request->buildings;
           $image->categoryId   =       $request->categoryId;
           $image->mainImage    =       $request->mainImage;
           $image->image        =       $request->image;
           $image->save();
           return response()->json(['message'=>'succesfully image uploaded']);
   }
}
