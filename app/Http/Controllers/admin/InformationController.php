<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Information;
class InformationController extends Controller
{
    public function getInformation($id){
            $information=Information::where('categoryId',$id)->get();
            $building= Information::all();
            return response()->json(['information'=>$information,'buildings'=>$building]);

    }
    public function postInformation(Request $request){
            $information= new Information;
            $information->categoryId=$request->categoryId;
            $information->name=$request->name;
            $information->email=$request->email;
            $information->url=$request->url;
            $information->phone=$request->phone;
            $information->address=$request->address;
            $information->latitude=$request->latitude;
            $information->longitude=$request->longitude;
            $information->services=json_encode($request->services);
            $information->description=$request->description;
            $information->contact_person=$request->contact_person;
            $information->contact_person_number=$request->contact_person_number;
            $information->save();
            return response()->json(['messsage '=>'Information added successfully','information'=>$information,'status'=>'Information SuccessFully Added !']);
    }


    public function updateInformation(Request $request){
        $updatedInformation = Information::find($request->updateId);
        // return response()->json($updatedInformation);

        $updatedInformation->categoryId = $request->categoryId;
        $updatedInformation->name=$request->name;
        $updatedInformation->email=$request->email;
        $updatedInformation->url=$request->url;
        $updatedInformation->phone=$request->phone;
        $updatedInformation->address=$request->address;
        $updatedInformation->latitude=$request->latitude;
        $updatedInformation->longitude=$request->longitude;
        $updatedInformation->services=json_encode($request->services);
        $updatedInformation->description=$request->description;
        $updatedInformation->contact_person=$request->contact_person;
        $updatedInformation->contact_person_number=$request->contact_person_number;
        $updatedInformation->save();
        return response()->json(['messsage '=>'Information updated successfully','information'=>$updatedInformation]);


    }

    public function deleteInformation(Request $request){
        $information= Information::find($request->id);
        $information->delete();
        return response()->json(['message'=>'successfully deleted']);
    }
}
