<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Hotel;
use App\Campus;

class CategoryController extends Controller
{
    //
    public function getInformation($id){
        $myid=$id;
        if($myid == 0){
            $hotel=Hotel::get();
            return response()->json(['info'=>'it is working with 0','hotel'=>$hotel]);
        }else{
            return response()->json(['info'=>'it is working with >0']);
        }

    }
    public function fetch(){
        $category=Category::get();
        return response()->json(['category'=>$category]);
    }

    public function add(Request $request){
       if(empty($request->id)){
        $category= new Category;
        $category->category=$request->category;
        $category->save();
        return response()->json(['message'=>'Category Added','category'=>$category,'status'=>0]);
       }
       else{
        $category= Category::find($request->id);
        $category->category=$request->category;
        $category->save();
        return response()->json(['message'=>'Category Updated','updatedcategory'=>$category,'status'=>1]);
       }
    }
}
