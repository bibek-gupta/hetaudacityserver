<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Information extends Model
{      
    use HasApiTokens;
    protected $table='informations';
    protected $fillable=[
        'categoryId',
        'name',
        'email',
        'url',
        'phone',
        'address',
        'latitude',
        'longitude',
        'services',
        'description',
        'contact_person',
        'contact_person_number',    
    ];
    public function getServicesAttribute($value){
      return json_decode($value,true);
    }
}
