<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class FeedBack extends Model
{
    use HasApiTokens;

 protected $table='feedbacks';
    protected $fillable=[
        'userId',
        'ratedStar',
        'profileId',
        'feedBack_title',
        'feedBack'
    ];

}
