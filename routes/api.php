<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login','AuthController@login');
Route::post('auth/register','AuthController@signup');



//Admin Routes start from here


Route::group([
    'prefix'=>'admin'],function(){
        //category
        Route::post('login','Admin\AuthenticationController@login');
        Route::post('addcategory','Admin\CategoryController@add');
        Route::get('getcategory','Admin\CategoryController@fetch');
        Route::get('getInformation/{id}','Admin\InformationController@getInformation');

        //category information
        Route::post('postInformation','Admin\InformationController@postInformation');
        Route::post('updateInformation','Admin\InformationController@updateInformation');
        Route::post('deleteInformation','Admin\InformationController@deleteInformation');
        Route::post('uploadImage','Admin\ImageController@uploadImage');

    });




    //Route For CLient Side


    Route::group([
        'prefix'=>'client'],function(){
            Route::get('getCategory','ClientController@getCategory');
            Route::get('getListOfBuildings','ClientController@getListOfBuildings');
            Route::post('getProfileInformation','ClientController@getProfileInformation');
            Route::middleware('auth:api')->post('getProfileDetails','ClientController@getProfileDetails');
            Route::get('search/{text}','ClientController@search');
            Route::middleware('auth:api')->post('postFeedBack','ClientController@postFeedBack');
        });