<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('categoryId');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('url');
            $table->string('phone');
            $table->string('address');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('services');
            $table->string('description');
            $table->string('contact_person');
            $table->string('contact_person_number');
            $table->foreign('categoryId')->references('id')->on('categories');
            $table->string('createdBy')->default('');
            $table->string('updatedBy')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informations');
    }
}
